#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;



int main(void)
{	
	int n,m,i,j,pos;
	char OnOff[105];
	
	while(scanf("%d",&n) && n)
	{	
		
		for(m=1;;m++){
			memset(OnOff,1,102); OnOff[0]=0; //The first region start powered off
			pos=0;
			for(i=1;pos!=12 && i<n ;i++){ //i represents the number of regions powered off
				for(j=0;j<m;){ //j is counting the number of valid steps
				        pos=(pos+1)%n;
					if(OnOff[pos])j++; //if a region is powered, then it is a valid step
				}
				OnOff[pos]=0;
				//printf("%d ",pos);
			}
			if(i==n)break;	
			//printf("\n");
		}
		printf("%d\n",m);
	}
	return 0;
}
