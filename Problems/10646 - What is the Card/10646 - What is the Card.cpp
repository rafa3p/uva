#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;


int main(void)
{
   	char deck [55][4],card;
	int i,j,k,cases,X,Y;
	scanf("%d",&cases);
	for(k=1;k<=cases;k++)
	{
		for(i=0;i<52;i++) scanf("%s",deck[i]);
		/*printf("\n");
		for(i=0;i<52;i++) printf("%s ",deck[i]);*/
		for(Y=0,i=26,j=0;j<3;j++)
		{	
			card = deck[i][0];
			if(card>='2' && card <= '9') X = card - '0';
			else X = 10;	
			Y+=X;
			i = i-11+X;
		}
		if(Y<=i) printf("Case %d: %s\n",k,deck[Y]);
		else printf("Case %d: %s\n",k,deck[Y+25-i]);
	}
	return 0;
}

