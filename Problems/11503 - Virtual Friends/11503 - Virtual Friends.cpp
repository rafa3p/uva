#include <string>
#include <cstdio>
#include <cstdlib>
#include <map>

using namespace std;

int sz[100005];
int id[100005];
map <string,int> index;

int Find(int x) {
    while(x != id[x]) {
        id[x] = id[id[x]];
        x = id[x];
    }
    return x;
}

int Union(int x, int y) { /*Return the size of the set*/
    x = Find(x);
    y = Find(y);
    if(x != y) {
        if(sz[x] < sz[y]) {
                sz[y] += sz[x];
                id[x] = y;
                return sz[y];
        }
        else{
            sz[x] += sz[y];
            id[y] = x;
            return sz[x];
        }
    }
    return sz[x];
}


int main(void){
    int t,e,idx;
    char p1[22],p2[22];
    scanf(" %d ",&t);
    while(t--){
        index.clear();
        idx = 0;
        scanf(" %d ",&e);
        while(e--){
            scanf(" %s %s ",p1,p2);
            if(!index.count(p1)){
                index[p1] = idx;
                sz[idx] = 1;
                id[idx] = idx;
                idx++;

            }
            if(!index.count(p2)){
                index[p2] = idx;
                sz[idx] = 1;
                id[idx] = idx;
                idx++;
            }
            printf("%d\n",Union(index[p1],index[p2]));
        }
    }
    return 0;
}
