#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

bool comp (int a, int b) {return a<b;}
int main (void)
{
	int ages[105];
	int n,i,j,k;
	while(scanf(" %d ",&n) && n){
		memset(ages,0,405);
		for(i=0;i<n;i++) { scanf(" %d ",&j); ages[j]++; }
			
		for(i=1;n;i++){
			k = ages[i];
			while(k-- > 0){			
				if(--n)printf("%d ",i);
				else printf("%d\n",i);
			}
		}
	}
	return 0;
}
