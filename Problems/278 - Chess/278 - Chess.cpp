#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

int main(void)
{
	int n,m,b,a,c;
	char p;
	scanf("%d",&c);
	while(c--)
	{
		scanf(" %c %d %d ",&p,&n,&m);
		if(p=='K')printf("%d\n", ((n+1)>>1)*((m+1)>>1));
		else if(p=='r' || p=='Q' ) printf("%d\n", (n<m)?n:m ); // min(n,m)
		else printf("%d\n", ((n*m)+1)>>1);
	}

	return 0;
}
