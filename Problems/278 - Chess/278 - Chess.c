#include<stdio.h>

int main(void)
{
	int n,m,b,a,c;
	char p;
	scanf("%d",&c);
	while(c--)
	{	
		scanf(" %c %d %d ",&p,&n,&m);
		if(p=='r' || p=='Q') printf("%d\n", (n<m)?n:m ); 
		else if( p=='K') printf("%d\n", ((n+1)>>1)*((m+1)>>1)); 
		else printf("%d\n", ((n*m)+1)>>1);
	}

	return 0;
}

