#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;


int main(void)
{
        int cases,a,b,c,i,j,k,turn,dice,player;
	int index[108],position[1000005];
	scanf(" %d ",&cases);
	while(cases--){
		scanf("%d %d %d",&a,&b,&c);
		memset(index,0,sizeof(index));
		for(k=0;k<a;k++)position[k]=1; 
		for(k=0;k<b;k++){ scanf(" %d ",&i); scanf(" %d ",&index[i]);}  
		for(k=0;k<c;k++)
		{
			scanf(" %d ",&dice);
			player = k%a;
			if(index[position[player]+dice] ) position[player] = index[position[player]+dice];
			else position[player]+=dice;
			if(position[player]>=100)
			{
				position[player]=100;
				while(++k < c) scanf(" %d ",&dice);
			}
		}
		for(k=0;k<a;k++) printf("Position of player %d is %d.\n",k+1,position[k]);
	}
        return 0;
}

