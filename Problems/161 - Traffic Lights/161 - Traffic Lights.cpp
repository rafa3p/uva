#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long

bool times[18100]={true};
void generate(int t)
{
	if(t==0) return;
	int ttt = t<<1 ;  int tt = t+5;
	for(int i=t-5;i<18090;i+=ttt)
		memset(times+i,0,tt); //setting the times that are red or oranje
}
int main(void)
{
	int j,t=1,min=100,h,m,s,i;
	int a,b,c;
	memset(times,1,18090);
	while(1){
		while(t && scanf(" %d ",&t) && t){
			
			if(min>t) min=t;
			generate(t);
		}
		for(i=min;i<18090;i++)
			if(times[i]) break;

		if(i>18000)printf("Signals fail to synchronise in 5 hours\n");
		else{
			//h = i/3600;	m=(i%3600)/60;	s= i%60;
			printf("%02d:%02d:%02d\n", i/3600,(i%3600)/60,i%60);
			
		}
		memset(times,1,18090);
		min=100;
		scanf(" %d  %d  %d ",&a,&b,&c);
		if((a+b+c) == 0) break;
		generate(a);
		generate(b);
		generate(c);
		if(a){
			if(c) { 
				t=1;
				(b<c)?min=b:min=c;
				if(a<min) min=a;
			}
			else{
				t = 0; (a<b)?min=a:min=b;
				//printf("%d %d !",a,b);
			}
		}
		else{
			t=1; 
			(b<c) ? min=b:min=c;
		}
		
	}
	return 0;
}
