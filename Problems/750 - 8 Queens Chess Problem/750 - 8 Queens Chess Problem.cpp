#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;
int x,y;
int v[9]; /*Vector with values from 1 to 8 to represent the queens' positions*/
int count;

bool checkSolution(int k){
    if(k==y && v[k]!=x) return false;
    for(int i=1;i<k;i++){
        if(  v[i] == v[k] ) return false; /*Same row*/
        if( (v[i]+i) == (v[k]+k) || (v[i]-i) == (v[k]-k)) return false;/*Same diagonal*/
    }
    return true;
}

/*k represents the number of queens on the actual board*/
void backtrack(int k){
    if(k==9){/*A SOLUTION !*/
        printf("%2d     ",++count);
        for(int i=1;i<9;i++)
            printf(" %d",v[i]);
        printf("\n");
        return;
    }
    for(int i=1;i<9;i++){
        v[k] = i;       /*Trying a solution*/
        if(checkSolution(k))
            backtrack(k+1);/*Fill the other rows*/
    }
}

int main(void){
    int t;
    scanf(" %d ",&t);
    while(t--){
        count = 0;
        memset(v,0,9);
        scanf(" %d %d ",&x,&y);
        printf("SOLN       COLUMN\n"
        " #      1 2 3 4 5 6 7 8\n\n");
        backtrack(1);
        if(t)printf("\n");
    }
    return 0;
}
