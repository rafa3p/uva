#include <cstdio>
#include <string.h>
#include <iostream>

using namespace std;

int main(void){
    char line[135];
    int sum=0;
    char c;
    while(fgets(line,135,stdin)!=NULL){
        int len = strlen(line);
        for(int i = 0;i<len;i++){
            c = line[i];
            if(c>='0' && c<='9'){
                sum +=  c-'0';
            }
            else if((c>='A' && c<='Z')  || c=='*'){
                for(int j=0;j<sum;j++)
                    printf("%c",c);
                sum = 0;
            }else if(c=='b'){
                for(int j=0;j<sum;j++)
                    printf(" ");
                sum = 0;
            }else if(c=='!' || c=='\n'){printf("\n");}
        }
    }
    return 0;
}
