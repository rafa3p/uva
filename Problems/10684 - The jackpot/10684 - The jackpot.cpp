#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

using namespace std;

int main(void){
    int input[10005];
    int n,i,maxSum,opt,tmp;

    /*OPT(i) = max(v[i], v[i]+OPT(i-1))*/

    while( scanf(" %d ",&n) && n){
        for(i = 0;i<n;i++)
            scanf(" %d ",&input[i]);

        opt = input[0];
        maxSum = (opt>0)?opt:0;

        for(i=1;i<n;i++){
            tmp = input[i];
            opt = (opt+tmp>tmp) ? opt+tmp:tmp;
            if(opt>maxSum) maxSum = opt;
        }

        if(maxSum)printf("The maximum winning streak is %d.\n",maxSum);
        else printf("Losing streak.\n");
    }

    return 0;
}
