#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;
bool comp(int a,int b){return a>b;}

int main(void)
{
	int i,j,k,x,r,c,m,n,s,max;
    char input[23];
    int counting[27];
	scanf(" %d ",&x);
	for(k=1;k<=x;k++)
    {

        memset(counting,0,108);

        scanf("%d%d%d%d",&r,&c,&m,&n);

        for(j=0;j<r;j++)
        {
            scanf("%s",input);
            for(i=0;i<c;i++){
                counting[input[i]-'A']++;
            }
        }

        sort(counting,counting+27,comp);
        s = max = counting[0];

        for(i=1;counting[i]==max;i++)
            s+=max;

        printf("Case %d: %d\n",k,s*m+(r*c - s)*n );

    }
    return 0;
}
