#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stack>
#include <string>
#include <bitset>
#include <map>

using namespace std;

int main(void){
    int t,n,k,i;
    char table[120];
    bool b;
    string number,input;
    map<string,int> telephones;
    map<string,int>::iterator p;
    table['0']='0';table['1']='1';
    table['A']=table['B']=table['C']=table['2'] = '2';
    table['D']=table['E']=table['F']=table['3'] = '3';
    table['G']=table['H']=table['I']=table['4'] = '4';
    table['J']=table['K']=table['L']=table['5'] = '5';
    table['M']=table['N']=table['O']=table['6'] = '6';
    table['P']=table['R']=table['S']=table['7'] = '7';
    table['T']=table['U']=table['V']=table['8'] = '8';
    table['W']=table['X']=table['Y']=table['9'] = '9';
    scanf(" %d ",&t);
    while(t--){
        scanf(" %d ",&n);
        while(n--){
            getline(cin,input);
            int l = input.length();
            number.erase (number.begin(),number.end());
            for(k = i = 0; k <l && i<7 ;k++ )
                if(input[k]!='-')
                    number.insert(number.begin()+i++,table[input[k]]);
            telephones[number]++;
        }
        b=false;
        for(p=telephones.begin();p!=telephones.end();p++){
            if(p->second>1){
                number = p->first;
                cout << number.substr(0,3) << '-' << number.substr(3,4);
                printf(" %d\n",p->second);
                b = true;
            }
        }
        if(b==false)printf("No duplicates.\n");
        if(t)printf("\n");
        telephones.clear();
    }
    return 0;
}
