#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

int vet[200010];
int seg[800010];

void create(int p,int a, int b){
    if(a==b) seg[p] = vet[a];
    else{
        create( (p<<1) , a , ((a+b)>>1) );
        create( (p<<1) +1 , ((a+b)>>1)+1 , b );
        seg[p] = seg[ p<< 1] + seg[ 1+(p<<1) ];
    }
}

void update(int p,int a,int b, int z, int v){
    if(a>z || b<z) return;
    if(a==b) seg[p] = v;
    else{
        update( (p<<1) , a , ((a+b)>>1),z,v );
        update( (p<<1) +1 , ((a+b)>>1)+1 , b,z,v );
        seg[p] = seg[ p<< 1] + seg[ 1+(p<<1) ];
    }
}

int query(int p,int a,int b, int i, int j){
    if(a>j || b<i) return 0;
    if(a>=i && b<=j) return seg[p];
    int x = query( (p<<1) , a , ((a+b)>>1) , i, j);
    int y = query(1 + (p<<1) , (1+((a+b)>>1)) , b , i, j);
    return x+y;
}

int main(void){
    int n,i,j,x,y;
    char in[4];
    for(i=1;scanf(" %d ",&n) && n;i++){
        if(i>1)printf("\n");
        printf("Case %d:\n",i);
        for(j=0;j<n;j++)
            scanf(" %d ",&vet[j]);
        create(1,0,n-1);
        while(scanf(" %s ",in) && in[0]!='E'){
            scanf(" %d %d ",&x,&y);
            if(in[0]=='M')
                printf("%d\n",query(1,0,n-1,x-1,y-1));
            else
                update(1,0,n-1,x-1,y);
        }
    }
    return 0;
}
