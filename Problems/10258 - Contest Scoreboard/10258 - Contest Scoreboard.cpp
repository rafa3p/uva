#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

typedef struct TEAM
{
    int solved,time,teamNumber;
    char ok; //flag to control if the team is participating
    bool problems[13];
    int problemsTime[13];
}TEAM;

bool compare(TEAM A,TEAM B)
{
    if(A.ok != B.ok ) return A.ok > B.ok;
    if (A.solved != B.solved) return A.solved > B.solved;
    if (A.time != B.time) return A.time < B.time;
    return A.teamNumber < B.teamNumber;
}

int main(void)
{
    int cases,i,j,t,p,time;
    char L,input[105];
    TEAM teams[103];
    scanf(" %d ",&cases);
    //getchar();
    while(cases--)
    {
        /*Initializating*/
        for(i=0;i<101;i++){
            teams[i].teamNumber = i;
            teams[i].ok=0;
            teams[i].time=0;
            teams[i].solved=0;
            for(j=0;j<13;j++) {
                teams[i].problems[j]=false;
                teams[i].problemsTime[j]=0;
            }
        }
        while(gets(input) && strlen(input))
        {
            sscanf(input,"%d %d %d %c",&t,&p,&time,&L);
            teams[t].ok=1;
            if(L=='C'){
                if(teams[t].problems[p]==false){
                    teams[t].problems[p] = true;
                    teams[t].solved++;
                    teams[t].time += teams[t].problemsTime[p] + time;
                }
            }
            else if(L=='I') {teams[t].problemsTime[p] += 20;}
        }
        sort(teams,teams+101,compare);
        for(i=0;teams[i].ok;i++) printf("%d %d %d\n",teams[i].teamNumber,teams[i].solved,teams[i].time);
        if(cases) printf("\n");
    }
}
