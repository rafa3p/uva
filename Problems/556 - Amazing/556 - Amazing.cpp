#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;

int xpos,ypos,x,y;
int a,b,i,j;
char mapp[1001][1001],counter[1000][1000],dir;

void changeDirection(){
	if(dir==0){
		if(mapp[ypos+1][xpos]==0) dir = 1;
		else if(mapp[ypos-1][xpos]==0) dir = 3;
		else dir =2;

	}else if(dir==1){
		if(mapp[ypos][xpos-1]==0) dir =2;
		else if(mapp[ypos][xpos+1]==0) dir = 0;
		else dir = 3;
	}else if(dir==2){
		if(mapp[ypos-1][xpos]==0) dir= 3;
		else if(mapp[ypos+1][xpos]==0) dir =1;
		else dir = 0;
	}else{
		if(mapp[ypos][xpos+1]==0) dir = 0;
		else if(mapp[ypos][xpos-1]==0) dir = 2;
		else dir = 1;
	}
}

bool hasNext(){
	//printf("%d %d  Dir:%d\n",ypos, xpos ,dir);
	if(dir==0) return (mapp[ypos][xpos+1]==0);
	if(dir==1) return (mapp[ypos+1][xpos]==0);
	if(dir==2) return (mapp[ypos][xpos-1]==0);
	return (mapp[ypos-1][xpos]==0);
}

void Next()
{
	
	counter[ypos-1][xpos-1]++;
	if(dir==0) xpos++;
	else if(dir==1) ypos++;
	else if(dir==2) xpos--;
	else ypos--;
}


void init(){
	char input[1000];
	for(i=0;i<a;i++)
		for(j=0;j<b;j++)
			counter[i][j] = 0;
	memset(mapp,1,b+2);
	for(j=1;j<=a;j++) 
		mapp[j][0] = mapp[j][b+1] = 1;
	for(j=0;j<=b+1;j++) 
		mapp[a+1][j] = 1;
	
	for(i=0;i<a;i++)
	{
		gets(input);
		for(j=0;j<b;j++)
			mapp[i+1][j+1] = input[j]-'0';
	}

}

void showmap()
{
	printf("MAP\n");
	for(i=0;i<a+2;i++){
		for(j=0;j<b+2;j++){
			printf("%d",mapp[i][j]);
		}
		printf("\n");
	}
	printf("CONT\n");
	for(i=0;i<a;i++){
		for(j=0;j<b;j++){
			printf("%d",counter[i][j]);
		}
		printf("\n");
	}
}



bool hasRight(){
	if(dir==0) return (mapp[ypos+1][xpos]==0);
	if(dir==1) return (mapp[ypos][xpos-1]==0);
	if(dir==2) return (mapp[ypos-1][xpos]==0);
	return (mapp[ypos][xpos+1]==0);
}

int main(void)
{
	int zero,one,two,three,four;
	while(scanf("%d %d ",&a,&b) && a)
	{
		init();
		
		xpos=1;ypos=a;dir=zero=one=two=three=four=0;
		do{
			if(hasRight()) dir = (dir+1)&3; //TurnRight();
			if(hasNext()) Next() ;
			else changeDirection() ;
			
		}while((xpos!=1) || (ypos!=a));
		//showmap();
		
		//counting
		for(i=0;i<a;i++){
			for(j=0;j<b;j++){
				if(counter[i][j]==0 && mapp[i+1][j+1]==0) zero++;
				else if(counter[i][j]==1)one++;
				else if(counter[i][j]==2)two++;
				else if(counter[i][j]==3)three++;
				else if(counter[i][j]==4) four++;
			}
		}
		printf("%3d%3d%3d%3d%3d\n",zero,one,two,three,four);
		//finish
		for(i=0;i<a+2;i++)
			memset(mapp[i],0,b+2);
	}
		

	
	return 0;
}


