#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
#define vi vector<int>
using namespace std;

int main(void)
{

	LL sieve_sieze=50000,upperbound=50000;
	bitset<50000> bs; 
	vi primes;
	int k,pos,a,b;


	bs.set();bs[0]=bs[1]=0;
	for(LL i = 2;i<=sieve_sieze;i++) if(bs[i]){
		//cross out multiples of i starting from i*i
		for(LL j=i*i;j<=sieve_sieze;j+=i) bs[j]=0;
		primes.push_back((int)i);

	}
	//primes is the list with prime numbers

	vi circle;
	while(scanf("%d",&k) && k)
	{
		circle.resize(k);

		for(a=0;a<k;a++) circle[a]=a+1;
		for(a=pos=0;a<(k-1);a++){ //each time kill one
			pos = (pos-1+primes[a]) % circle.size();
			circle.erase(circle.begin()+pos);
		}
		printf("%d\n",circle[0]);
	}

	return 0;
}
