#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long

int main(void)
{
	int p,i,j,a,sheets;
	while(scanf(" %d ",&p)&& p)
	{
		printf("Printing order for %d pages:\n",p);
		if(p==1) printf("Sheet 1, front: Blank, 1\n");
		else{
			a = p&3; //a = p%4
			if(a){ 
				sheets = (p>>2) + 1;
				if(a==2){
					printf("Sheet 1, front: Blank, 1\nSheet 1, back : 2, Blank\n");
					i=3;j=2;
				}
				else if(a==3){
					printf("Sheet 1, front: Blank, 1\nSheet 1, back : 2, %d\n",p);
					p--;i=3;j=2;
				}
				else{
					printf("Sheet 1, front: Blank, 1\nSheet 1, back : 2, Blank\n");
					printf("Sheet 2, front: Blank, 3\nSheet 2, back : 4, %d\n",p);
					p--;j=3;i=5;
				}
				for(;j<=sheets;i+=2,j++,p-=2)
					printf("Sheet %d, front: %d, %d\nSheet %d, back : %d, %d\n",j,p,i,j,i+1,p-1);
			}
			else{ 
				sheets = p>>2 ;
				for(i=j=1;j<=sheets;i+=2,j++,p-=2)
					printf("Sheet %d, front: %d, %d\nSheet %d, back : %d, %d\n",j,p,i,j,i+1,p-1);
			}
		}
	}
	return 0;
}
