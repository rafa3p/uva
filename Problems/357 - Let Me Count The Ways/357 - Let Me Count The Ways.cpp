#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

#define M 30000
int main(void)
{
    int c;
    LL ways[M+10];
    fill(ways,ways+M+10,1);
    for(int j = 5; j<=M; j++) ways[j] += ways[j-5];
    for(int j = 10; j<=M; j++) ways[j] += ways[j-10];
    for(int j = 25; j<=M; j++) ways[j] += ways[j-25];
    for(int j = 50; j<=M; j++) ways[j] += ways[j-50];

    while(scanf(" %d ",&c)==1){
        c<5?printf("There is only 1 way to produce %d cents change.\n",c):
            printf("There are %lld ways to produce %d cents change.\n",ways[c],c);
    }
    return 0;
}

