#include <cstdio>
#include <cstdlib>
#include <map>

using namespace std;

struct trie{
    map<char,trie> m;
    bool endOfWord;
    trie(){endOfWord=false;}
};

trie root;

void insert(trie &t,char *s){
    if(*s == '\0'){
        t.endOfWord = true;
        return;
    }
    insert(t.m[*s],s+1);
}

unsigned int query(trie &t,unsigned int k){
    map<char,trie> :: iterator it = t.m.begin();
    unsigned int sum = 0;
    if(t.endOfWord){
        for(;it!=t.m.end();it++)
            sum+= query(it->second,k+1);
        return sum + k;
    }
    if(t.m.size()==1) return query(it->second,k);
    for(;it!=t.m.end();it++)
        sum+= query(it->second,k+1);
    return sum;
}

int main(void){
    int n;
    unsigned int sum;
    char s[85];
    map<char,trie> :: iterator it;

    while(scanf(" %d ",&n) != EOF){
        root.m.clear();
        for(int i = 0 ; i < n; i++){
            scanf(" %s ",s);
            insert(root,s);
        }
        sum = 0;
        for(it=root.m.begin();it!=root.m.end();it++)
            sum += query(it->second,1);
        printf("%.2f\n",(double)sum/n);
    }
    return 0;
}

