#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;
bool comp(int a,int b){return a<b;}
int main(void)
{
    int T,S,A,F,i,a,b;
    int p1,p2;
    int v1[50005],v2[50005];
    scanf(" %d ",&T);
    while(T--)
    {
        scanf(" %d %d %d ",&S,&A,&F);
        for(i=0;i<F;i++)
        	scanf(" %d %d ",&v1[i],&v2[i]);
       
        
        sort(v1,v1+F,comp);
        sort(v2,v2+F,comp);

        printf("(Street: %d, Avenue: %d)\n",v1[(F-1)>>1],v2[(F-1)>>1]);

    }
    return 0;
}
