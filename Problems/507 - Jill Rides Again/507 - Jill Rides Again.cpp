#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

int main(void)
{
    int b,s,temp,beg,sum;
    int optSum,optEnd,optBegin;
    scanf(" %d ",&b);
    for(int r = 1;r<=b;r++){
        scanf(" %d ",&s);

        optSum = optBegin = optEnd = sum = beg = 0;

        for(int i = 1; i < s ; i++){
            scanf(" %d ",&temp);
            sum += temp;

            if(sum > optSum || (sum==optSum && i - beg  > optEnd - optBegin) ){
                optBegin = beg;
                optEnd = i;
                optSum = sum;
            }
            if(sum < 0){
                sum = 0;
                beg = i;
            }
        }
         optSum > 0 ? printf("The nicest part of route %d is between stops %d and %d\n",r,optBegin+1,optEnd+1) :
            printf("Route %d has no nice parts\n",r);
    }
    return 0;
}

