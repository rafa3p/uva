#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;


int main(void)
{	
	int vet[] = {0,2,7,5,30,169,441,1872,7632,1740,93313,459901,1358657,2504881};
	
	int a;
	while(scanf("%d",&a) && a)
		printf("%d\n",vet[a]);        
	
	return 0;
}

