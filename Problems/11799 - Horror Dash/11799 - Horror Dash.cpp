#include <cstdio>
//Coment...It was faster when using C
//0.008 for C++ and 0.004 for C
//Just changed <cstdio> by stdio.h

int main(void)
{
    int t,n,i,s,max;
    scanf("%d",&t);
    for(i=1;i<=t;i++)
    {
	max=0;
	scanf("%d",&n);
	while(n--)
	{
	    scanf("%d",&s);
	    if(s>max) max=s;
	}
	printf("Case %d: %d\n",i,max);
    }
    return 0;
}
