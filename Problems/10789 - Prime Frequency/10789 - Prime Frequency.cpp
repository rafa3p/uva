#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;
#define N 2005

int main(void)
{
    int t;
    bool empty;
    bitset <N> isPrime;
    string line;
    map<char,int>::iterator it,itEnd;

    /*Processing bitset*/
    isPrime.set();
    isPrime[0] = isPrime[1] = 0;
    int top = ceil(sqrt(N));
    for(int i = 2; i<= top ;i++) if(isPrime[i]){
        for(int j = i*i; j<=N;j+=i)
            isPrime[j] = 0;
    }

    scanf(" %d ",&t);
    for(int k =1;k<=t;k++){
        empty = true;
        getline(cin,line);
        map <char,int> myMap;

        int sz = line.size();
        for(int i = 0;i<sz;i++) myMap[line[i]]++;

        printf("Case %d: ",k);
        for (map<char,int>::iterator it=myMap.begin(); it!=myMap.end(); ++it){
            if(isPrime[it->second]){
                /*printf("%c",it->first);*/
                putchar(it->first);
                empty = false;
            }
        }
        empty?printf("empty\n"):putchar('\n');
    }
    return 0;
}

/*OBS : putchar() > printf("%c")*/
