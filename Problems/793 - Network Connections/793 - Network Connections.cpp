#include <string>
#include <cstring>
#include <cstdio>
#include <cctype>
#include <cstdlib>
#include <map>

using namespace std;

int sz[1000000];
int id[1000000];

 int Find(int x) {
    while(x != id[x]) {
        id[x] = id[id[x]];
        x = id[x];
    }
    return x;
}

void Union(int x, int y) {
    x = Find(x);
    y = Find(y);
    if(x != y) {
        if(sz[x] < sz[y]) {
                sz[y] += sz[x];
                id[x] = y;
        }
        else{
            sz[x] += sz[y];
            id[y] = x;
        }
    }
}

int main(void){
    int t,n,c1,c2,s,u;
    char c;
    scanf(" %u ",&t);
    while(t--){
        scanf(" %d ",&n);

        /*Data inicialization*/
        s = u = 0;
        memset(sz,1,n*sizeof(int));
        for(int i = 1;i<=n;i++) id[i] = i;

        while( (c = getchar()) && (c=='c' || c=='q')){
            if(c=='c'){
                scanf("%d %d",&c1,&c2);getchar();
                Union(c1,c2);
            }
            else{
                scanf("%d %d",&c1,&c2);getchar();
                if(Find(c1)==Find(c2)) s++;
                else u++;
            }
        }
        printf("%d,%d\n",s,u);
        if(t)printf("\n");
    }
    return 0;
}
