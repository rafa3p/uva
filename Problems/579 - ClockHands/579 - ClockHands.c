#include <stdio.h>

int main(void)
{
	int H,M;
	double angle,Hangle,Mangle;
	while(scanf(" %d:%d ",&H,&M) && (H|M))
	{
		Hangle = H*30 + M/2.0;
		Mangle = (M<<2) + (M<<1);
		angle = (Hangle>Mangle)?(Hangle-Mangle):(Mangle-Hangle);
		if(angle<180) printf("%.3f\n",angle);
		else printf("%.3f\n",360.0-angle);
	}
	return 0;
}
