#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;

int main(void)
{

	char input[300];
	while(scanf("%s",input) && input[0]!='#')
	{
		if(next_permutation(input,input+strlen(input))) printf("%s\n",input);
		else printf("No Successor\n");
	}
	return 0;
}
