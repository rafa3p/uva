#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

int main (void)
{
    int t,a;
    char input[35];
    map <string,int> mp;
    scanf(" %d ",&t);
    while(t--)
    {
        a = 0;
        while(gets(input) && strlen(input))
        {
            //printf("%s\n",input);
            mp[input]++;
            a++;
        }

        for (map<string, int>::iterator it = mp.begin(); it != mp.end(); it++)
            printf("%s %.4f\n", ((string)it->first).c_str(),(double) 100*it->second/a);
        if(t) { mp.clear(); printf("\n");}
    }
    return 0;
}
