#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;

int main(void)
{
	int m,n,i,j,k=1;
	char counting[103][103];	
	char field[103][103];
	bool first = false;
	while(scanf("%d %d",&n,&m) && (n|m)){
		for(i=1;i<=n;i++) scanf("%s",field[i]);	
		memset(counting,0,sizeof(counting));

		if(first)printf("\n");
		first = true;

		for(i=1;i<=n;i++)
		{
			for(j=0;j<m;j++)
			{
				if(field[i][j]=='*')
				{
					counting[i-1][j]++;   counting[i-1][j+1]++;   counting[i-1][j+2]++;
					counting[i][j]++;             /*[i][j+1]*/    counting[i][j+2]++;
					counting[i+1][j]++;   counting[i+1][j+1]++;   counting[i+1][j+2]++;
				}
			}
		}
		printf("Field #%d:\n",k++);
		for(i=1;i<=n;i++)
		{
			for(j=0;j<m;j++)
			{
				if(field[i][j]=='*')  printf("*");
				else   printf("%c",counting[i][j+1]+'0' );
			}
			printf("\n");
		}
	}
	return 0;
}
