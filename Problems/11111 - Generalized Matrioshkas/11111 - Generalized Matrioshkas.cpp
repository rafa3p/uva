#include <cstdio>
#include <iostream>
#include <stack>
#include <string>
#include <string.h>
using namespace std;

int main(void){
    char line[100000];
    stack< pair <int,int> > toys; /*toy size and previous clearance*/
    int clearance,toy,pos,inc,sum;
    bool matrioska;
    while(gets(line)){
        matrioska = true;
        pos = sum = 0;
        while(sscanf(&line[pos],"%d%n",&toy,&inc) != EOF){
            sum+=toy;
            if(toy>0){
                if(toys.empty() || toys.top().first != -toy){ //nested problem
                    matrioska = false;
                    break;
                }else{ //positive toy : removing toy from stack
                    toys.pop();
                    if(!toys.empty()){
                        clearance = toys.top().second - toy;
                        if(clearance <= 0 ){ //lack of space : previous toy is full
                            matrioska = false;
                            break;
                        }
                        toys.top().second = clearance;
                    }
                }
            }else{
                if(toys.empty())toys.push(make_pair(toy,-toy)); //first toy
                else{
                    if(toys.top().first > toy){ //lack of space : toy bigger than previous
                        matrioska = false;
                        break;
                    }
                    toys.push(make_pair(toy,-toy));
                }
            }
            pos+=inc;
        }
        while(!toys.empty())toys.pop();
        if(pos!=0){
            if(sum==0 && matrioska) printf(":-) Matrioshka!\n");
            else printf(":-( Try again.\n");
        }else if(sum!=0)printf(":-( Try again.\n");
    }
    return 0;
}
