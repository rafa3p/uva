#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
#define vi vector<int>
using namespace std;

int main(void)
{
	map <string,string> Sort;
	map <string,int> Counter;
	char input[81],a,aux[81];
	int i,j,size;
	while(scanf("%s",input) && input[0] != '#'){
		size = strlen(input);
		for(i=0;i<size;i++)
 		{
			aux[i] = tolower(input[i]); //Insertion Sort
			for(j=i ; j>0 && aux[j]<aux[j-1];j--) 
			{
				//swap

				a  =  aux[j-1];
				aux[j-1]  =  aux[j];
				aux[j]  =  a;
			}
		}
		aux[i]	= '\0';
		Sort[input] = aux;
		Counter[aux]++;
	}
	
	for(map <string,string> ::iterator i = Sort.begin();i!=Sort.end();i++)
		if(Counter[i->second] == 1) printf("%s\n",i->first.c_str());


	return 0;
}


