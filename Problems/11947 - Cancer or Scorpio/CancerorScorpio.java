import java.util.*;

public class CancerorScorpio { 
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		Calendar cal = Calendar.getInstance();
		int n = scan.nextInt();
		int DD,MM,YYYY,Inp;
		int i;
		String Zodiac;
		for(i=1;i<=n;i++){
			Inp = scan.nextInt();
			YYYY = Inp%10000;
			DD = (Inp/10000)%100;
			MM = Inp/1000000;	
			//System.out.println(DD + " " + MM + " " + YYYY);
			cal.set(YYYY,MM-1,DD); //Java's Calendar using month from index 0, while the date is from index 1.
			cal.add(Calendar.DATE,280); //advance 40 weeks (40*7)
			
			//get new date
			YYYY = cal.get(Calendar.YEAR);
			DD = cal.get(Calendar.DATE);
			MM = cal.get(Calendar.MONTH)+1;
			
			Zodiac = "capricorn";
			if(cal.after(new GregorianCalendar(YYYY,11,23))) Zodiac = "capricorn";
			else if (cal.after(new GregorianCalendar(YYYY,10,23))) Zodiac = "sagittarius";
			else if (cal.after(new GregorianCalendar(YYYY,9,24))) Zodiac = "scorpio";
			else if (cal.after(new GregorianCalendar(YYYY,8,24))) Zodiac = "libra";
			else if (cal.after(new GregorianCalendar(YYYY,7,22))) Zodiac = "virgo";
			else if (cal.after(new GregorianCalendar(YYYY,6,23))) Zodiac = "leo";
			else if (cal.after(new GregorianCalendar(YYYY,5,22))) Zodiac = "cancer";
			else if (cal.after(new GregorianCalendar(YYYY,4,22))) Zodiac = "gemini";
			else if (cal.after(new GregorianCalendar(YYYY,3,21))) Zodiac = "taurus";
			else if (cal.after(new GregorianCalendar(YYYY,2,21))) Zodiac = "aries";
			else if (cal.after(new GregorianCalendar(YYYY,1,20))) Zodiac = "pisces";
			else if (cal.after(new GregorianCalendar(YYYY,0,21))) Zodiac = "aquarius";
			
			System.out.printf("%d %02d/%02d/%04d %s\n",i,MM,DD,YYYY,Zodiac);
			
		}
	}
	
}


