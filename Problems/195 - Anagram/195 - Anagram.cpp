#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
#define vi vector<int>
using namespace std;

bool comp (char a,char b) { 
	char c = tolower(a);
	char d = tolower(b);
	return (d==c)?(a<b):(c<d);
	
}
int main(void)
{
	int t,len;
	char input[100];
	scanf("%d",&t);
	while(t--)
	{
		scanf("%s",input);
		len = strlen(input);
		sort(input,input+len,comp);
		do{
			printf("%s\n",input);
		}	while(next_permutation(input,input+len,comp));
	}
	return 0;
}
