#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

int main(void)
{
	int n,m,b,a,p;
	while(scanf("%d %d",&n,&m) && n)
	{
		if(n<m){a=n;b=m;}
		else {a=m;b=n;} //a = min(n,m) 	b = max(n,m) 

		if(a==1) printf("%d knights may be placed on a %d row %d column board.\n",b,n,m);
		else if(a==2)
		{ 
			int rest = b%4;
			if(rest!=3) printf("%d knights may be placed on a %d row %d column board.\n",((b>>2)<<2)+(rest<<1),n,m);
			else printf("%d knights may be placed on a %d row %d column board.\n",((b>>2)<<2)+4,n,m);
		}
		else
		{
			/*Half of the board is black. If a knight is on a black square, than he just can go to a white square!	*/
			printf("%d knights may be placed on a %d row %d column board.\n",(((a*b)+1)>>1),n,m);
			
		}
	}

	return 0;
}
