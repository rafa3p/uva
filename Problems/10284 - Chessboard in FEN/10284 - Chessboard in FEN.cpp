#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

char table[8][8];
bool NOPIECE(int a, int b){return (table[a][b] == '0' || table[a][b] == '1');}
bool ATTACK(int a,int b){return (table[a][b] = '1');}
bool LEGAL(int a,int b){return (a >= 0 && b >= 0 && a < 8 && b < 8);}


void Modify(char piece, int x, int  y)
{
	int i,j;
	if(piece == 'p')
	{
		if(LEGAL(x+1,y+1) && NOPIECE(x+1,y+1)) ATTACK(x+1,y+1);
		if(LEGAL(x+1,y-1) && NOPIECE(x+1,y-1)) ATTACK(x+1,y-1);
	}
	else if(piece=='P')
	{
		if(LEGAL(x-1,y+1) && NOPIECE(x-1,y+1)) ATTACK(x-1,y+1);
		if(LEGAL(x-1,y-1) && NOPIECE(x-1,y-1)) ATTACK(x-1,y-1);
	}	
	else if(piece=='Q' || piece=='q' )
	{
		//Root moves
		for(i=x+1;LEGAL(i,y) && NOPIECE(i,y) ;i++) ATTACK(i,y);
		for(i=x-1;LEGAL(i,y) && NOPIECE(i,y) ;i--) ATTACK(i,y);
		for(i=y+1;LEGAL(x,i) && NOPIECE(x,i) ;i++) ATTACK(x,i);
		for(i=y-1;LEGAL(x,i) && NOPIECE(x,i) ;i--) ATTACK(x,i);

		//Bishop moves
		for(i=x+1,j=y+1;LEGAL(i,j) && NOPIECE(i,j) ;i++,j++) ATTACK(i,j);
		for(i=x+1,j=y-1;LEGAL(i,j) && NOPIECE(i,j) ;i++,j--) ATTACK(i,j);
		for(i=x-1,j=y+1;LEGAL(i,j) && NOPIECE(i,j) ;i--,j++) ATTACK(i,j);
		for(i=x-1,j=y-1;LEGAL(i,j) && NOPIECE(i,j) ;i--,j--) ATTACK(i,j);
	}
	else if(piece=='R' || piece=='r' )
	{
		for(i=x+1;LEGAL(i,y) && NOPIECE(i,y) ;i++) ATTACK(i,y);
		for(i=x-1;LEGAL(i,y) && NOPIECE(i,y) ;i--) ATTACK(i,y);
		for(i=y+1;LEGAL(x,i) && NOPIECE(x,i) ;i++) ATTACK(x,i);
		for(i=y-1;LEGAL(x,i) && NOPIECE(x,i) ;i--) ATTACK(x,i);
	}
	else if(piece=='b' || piece=='B' )
	{
		for(i=x+1,j=y+1;LEGAL(i,j) && NOPIECE(i,j) ;i++,j++) ATTACK(i,j);
		for(i=x+1,j=y-1;LEGAL(i,j) && NOPIECE(i,j) ;i++,j--) ATTACK(i,j);
		for(i=x-1,j=y+1;LEGAL(i,j) && NOPIECE(i,j) ;i--,j++) ATTACK(i,j);
		for(i=x-1,j=y-1;LEGAL(i,j) && NOPIECE(i,j) ;i--,j--) ATTACK(i,j);
	}
	else if(piece=='k' || piece=='K' )
	{
		if(LEGAL(x-1,y-1) && NOPIECE(x-1,y-1)) ATTACK(x-1,y-1);
		if(LEGAL(x-1,y) && NOPIECE(x-1,y)) ATTACK(x-1,y);
		if(LEGAL(x-1,y+1) && NOPIECE(x-1,y+1)) ATTACK(x-1,y+1);
		
		if(LEGAL(x+1,y-1) && NOPIECE(x+1,y-1)) ATTACK(x+1,y-1);
		if(LEGAL(x+1,y) && NOPIECE(x+1,y)) ATTACK(x+1,y);
		if(LEGAL(x+1,y+1) && NOPIECE(x+1,y+1)) ATTACK(x+1,y+1);
		
		if(LEGAL(x,y+1) && NOPIECE(x,y+1)) ATTACK(x,y+1);
		if(LEGAL(x,y-1) && NOPIECE(x,y-1)) ATTACK(x,y-1);
	}
	else
	{
		if(LEGAL(x+2,y+1) && NOPIECE(x+2,y+1)) ATTACK(x+2,y+1);
		if(LEGAL(x+2,y-1) && NOPIECE(x+2,y-1)) ATTACK(x+2,y-1);
		if(LEGAL(x+1,y+2) && NOPIECE(x+1,y+2)) ATTACK(x+1,y+2);
		if(LEGAL(x+1,y-2) && NOPIECE(x+1,y-2)) ATTACK(x+1,y-2);
		if(LEGAL(x-2,y+1) && NOPIECE(x-2,y+1)) ATTACK(x-2,y+1);
		if(LEGAL(x-2,y-1) && NOPIECE(x-2,y-1)) ATTACK(x-2,y-1);
		if(LEGAL(x-1,y+2) && NOPIECE(x-1,y+2)) ATTACK(x-1,y+2);
		if(LEGAL(x-1,y-2) && NOPIECE(x-1,y-2)) ATTACK(x-1,y-2);	
	}
}

int main(void)
{
	char input[150];
	int i,j,k,l,n,m; 
	//0 - unoccupied/unattacked    1 - Attacked   Else - Occupied by a piece			
		
	while(scanf("%s",input))
	{
		
		//Initializatig the map
		memset(table,'0',64);
		for(i=0,j=0,k=0;input[i]!='\0';i++)
		{
			if(input[i]>='1' && input[i]<='8') j+= (input[i]-'0');
			else if(input[i]=='/') k++;
			else { table[k][j%8] = input[i]; j++;}
		}

		for(i=0;i<8;i++)
		{
			for(j=0;j<8;j++)
			{
				if(table[i][j] == '0' || table[i][j] == '1' ) continue;
				else Modify(table[i][j],i,j);
			}
		}
		for(i=l=0;i<8;i++)
		{
			for(j=0;j<8;j++)
			{
				if(table[i][j] == '0')l++; 
			}
		}
		printf("%d\n",l);
	}
	return 0;
	
}
