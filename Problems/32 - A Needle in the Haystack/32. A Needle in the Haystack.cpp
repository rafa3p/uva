#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>

using namespace std;

vector <int> B;
string N,H;/*Needle and haystack*/
int n,m; /*Needle and haystack size*/

void kmpPreprocess(){
    int i = 0, j = -1;B[0] = -1;
    while(i<n){
        while(j>=0 && N[i] != N[j])
            j = B[j];
        i++;j++;
        B[i] = j;
    }
}

void debug(){
    printf("%d %d\n",n,m);
    cout << N << endl;
    cout << H << endl;
}

void kmpSearch(){
    int i = 0, j = 0;
    bool match = false;
    while(i<m){
        while(j>=0 && H[i] != N[j])
            j = B[j];
        i++;j++;
        if(j==n){
            printf("%d\n",i-j);
            j = B[j];
            match = true;
        }
    }
    if(!match)putchar('\n');
}
int main(void){
    while(scanf(" %d ",&n) != EOF){
        getline(cin,N);
        getline(cin,H);
        m = H.length();
        B = vector<int>(n+1);
        kmpPreprocess();
        /*debug();*/
        kmpSearch();
    }
    return 0;
}

