#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;


int main(void)
{
    int i,j,k,NN,Cases,Shuffles[102][55],Index[55],OldIndex[55];	
    vector <string> Deck (55);
    char input[50];
    bool blank = false; 
    
    //Initializing the Deck
    string suits[] = {"Clubs","Diamonds","Hearts","Spades"};
    string values[] = {"2","3","4","5","6","7","8","9","10","Jack","Queen","King","Ace"};
    
    for(i=0,k=1;i<4;i++)
	for(j=0;j<13;j++) 
	    Deck[k++]  = values[j] + " of " + suits[i];
    
    //Deck Initialized - OK
 
    scanf("%d",&Cases);
    while(Cases--){
	
        scanf("%d",&NN); //Number of shuffles the dealer knows

	for(i=1;i<=NN;i++)
	    for(j=1;j<53;j++) 
	        scanf("%d",&Shuffles[i][j]);
 	for(i=1;i<=52;i++) OldIndex[i] = i; //There is not the card 'zero'

	getchar();//get \n
	//swapping
        while(gets(input) && strlen(input)){
	    k = atoi(input);
	    
	    for(i=1;i<53;i++) Index[i] = OldIndex[ Shuffles[k][i] ];
	    for(i=1;i<53;i++) OldIndex[i] = Index[i];
	}
	if(blank) printf("\n");
        	blank = true; 
        for(i=1;i<53;i++) cout << Deck[Index[i]] << endl ;
	
    }
     return 0;
}

