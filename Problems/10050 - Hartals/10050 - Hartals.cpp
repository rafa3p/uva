#include <cstdio>
#include <iostream>
#include <stack>
#include <string>
#include <bitset>

using namespace std;

int main(void){
    int t,n,p,k,day;
    bitset <3680> hartals;
    scanf(" %d ",&t);
    while(t--){
        scanf(" %d ",&n);
        scanf(" %d ",&p);
        while(p--){
            scanf(" %d ",&k);
            for(int i = k; i<=n; i+=k ){
                day = i%7;
                if(day!=0 && day!=6)
                    hartals.set(i);
            }
        }
        printf("%d\n",hartals.count());
        hartals.reset();
    }
    return 0;
}
