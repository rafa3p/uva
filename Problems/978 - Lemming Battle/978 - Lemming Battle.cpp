#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <queue>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long

using namespace std;
int main(void)
{
	int SG,SB,t,G,B,j,CG,CB,BA;
	priority_queue <int> green;
	priority_queue <int> blue;
	queue <int> GR,BL;
	double g_str,b_str;

	scanf(" %d ",&t);
	while(t--)
	{
		g_str = b_str = 0;
		scanf(" %d %d %d ",&BA,&SG,&SB);
		G = SG;
		while(G--){
			scanf(" %d ",&j);
			green.push(j);
			g_str += j;
		}
		B = SB;
		while(B--){
			scanf(" %d ",&j);
			blue.push(j);
			b_str+=j;
		}
		if(g_str == b_str) printf("green and blue died\n");
		else{
			while(!( green.empty() || blue.empty() )){
				
				for(j=0;j<BA;j++)
				{
					
					if(green.empty()) G = 0;
					else { G = green.top();green.pop();}
					if(blue.empty()) B = 0;
					else { B = blue.top();blue.pop();}
					if(B>G) BL.push(B-G);
					else if(G>B) GR.push(G-B);
				}
				while(!BL.empty()){ blue.push(BL.front());BL.pop();  }
				while(!GR.empty()){ green.push(GR.front());GR.pop() ; }
			}
			if(green.empty()){
				printf("blue wins\n");
				while(!blue.empty()){
					printf("%d\n",blue.top());
					blue.pop();
				}
			}
			else{
				printf("green wins\n");
				while(!green.empty()){
					printf("%d\n",green.top());
					green.pop();
				}
			}
		}
		if(t)printf("\n");
	}	
	return 0;
}
