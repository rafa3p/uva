#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

struct obj{
    int val;
    int pos;
};

int vet[100001];
obj seg[400001];

void create(int p,int a, int b){
    if(a==b) {seg[p] = (obj){vet[a],a};}
    else{
        create( (p<<1) , a , ((a+b)>>1) );
        create( (p<<1) +1 , ((a+b)>>1)+1 , b );
        seg[p] = (obj)((seg[p<<1].val > seg[1+(p<<1)].val)? (seg[p<<1]) : (seg[1+(p<<1)]) );
    }
}

obj query(int p,int a,int b, int i, int j){
    if(a>j || b<i) return (obj){-1,-1};
    if(a>=i && b<=j) return seg[p];
    obj x = query( (p<<1) , a , ((a+b)>>1) , i, j);
    obj y = query(1 + (p<<1) , (1+((a+b)>>1)) , b , i, j);
    return (obj)((x.val > y.val)?x:y);
}

void update(int p,int a,int b, int z, int v){
    if(a>z || b<z) return;
    if(a==b) {seg[p] = (obj){v,a};}
    else{
        update( (p<<1) , a , ((a+b)>>1),z,v );
        update( (p<<1) +1 , ((a+b)>>1)+1 , b,z,v );
        seg[p] = (obj)((seg[p<<1].val > seg[1+(p<<1)].val)? (seg[p<<1]) : (seg[1+(p<<1)]) );
    }
}

int main(void){
    int n,q,i,x,y;
    obj first,sec;
    char c;
    scanf(" %d ",&n);
    for(i=0;i<n;i++)scanf(" %d ",&vet[i]);
    create(1,0,n-1);
    scanf(" %d ",&q);
    while(q--){
        scanf(" %c %d %d ",&c,&x,&y);
        if(c=='Q'){
            first = query(1,0,n-1,x-1,y-1);
            update(1,0,n-1,first.pos,-1);
            sec = query(1,0,n-1,x-1,y-1);
            update(1,0,n-1,first.pos,first.val);
            printf("%d\n",first.val+sec.val);
        }
        else if(c=='U'){
            update(1,0,n-1,x-1,y);
        }
        else break;
    }
    return 0;
}
