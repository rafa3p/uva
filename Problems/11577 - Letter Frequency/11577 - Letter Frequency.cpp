#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

int main(void)
{
    int t;
    char line[205];
    int freq[125];
    scanf(" %d ",&t);
    while(t--){
        memset(freq+'a',0,26*sizeof(int));
        int max = 0;
        gets(line);
        int sz = strlen(line);
        for(int i = 0;i<sz;i++)
            freq[tolower(line[i])]++;
        for(int i = 'a';i<='z';i++)
            if(freq[i]>max)max=freq[i];
        for(int i = 'a';i<='z';i++)
            if(freq[i]==max)putchar(i);
        putchar('\n');
    }
    return 0;
}

