#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define pi 3.141592653589793
#define LL unsigned long long

using namespace std;

long double dist(long double x1,long double y1,long double x2,long double y2){
    long double dx = x2-x1;
    long double dy = y2-y1;
    return sqrt(dx*dx+dy*dy);
}

/*
p = a + b + c
s = p * 0.5
A = sqrt ( s*(s-a)*(s-b)*(s-c)) -> Heron's Formula
A = 0.5*fabs(x1*y2+x2*y3+x3*y1-y1*x2-y2*x3-y3*x1) -> cross product
R = a*b*c / 4 * A
D = 2 * pi * R
*/
int main(void)
{
    long double x1,x2,x3,y1,y2,y3;
    while(scanf(" %Lf %Lf %Lf %Lf %Lf %Lf ",&x1,&y1,&x2,&y2,&x3,&y3)==6){
        long double a = dist(x1,y1,x2,y2);
        long double b = dist(x1,y1,x3,y3);
        long double c = dist(x3,y3,x2,y2);
        long double A = 0.5*fabs(x1*y2+x2*y3+x3*y1-y1*x2-y2*x3-y3*x1);
        long double d = 0.5*pi*a*b*c / A;
        printf("%.2Lf\n",d);
    }
    return 0;
}

