#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define pi acos(-1.0)
#define LL unsigned long long
using namespace std;

int main(void)
{
    char table[255];
    char exp[35];
    table['A'] = table['B'] = table['C'] = '2';
    table['D'] = table['E'] = table['F'] = '3';
    table['G'] = table['H'] = table['I'] = '4';
    table['J'] = table['K'] = table['L'] = '5';
    table['M'] = table['N'] = table['O'] = '6';
    table['P'] = table['Q'] = table['R'] =  table['S'] = '7';
    table['T'] = table['U'] = table['V'] = '8';
    table['W'] = table['X'] = table['Y'] = table['Z'] = '9';
    table['0'] = '0';
    table['1'] = '1';
    table['-'] = '-';
    while(scanf(" %s ",exp) == 1) {
        int len = strlen(exp);
        for(int i = 0;i<len;i++)
            exp[i] = table[exp[i]];
        printf("%s\n",exp);
    }
    return 0;
}

