#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;


int main(void)
{
   	char hand[14][3],card,suit;
	char F[100]; //flag to verify if the suit has stopped ( CS = Club Stopped)
	int i,j,k,Points;
	char C[100]; //Counting the number of cards of each suit;

	 while(scanf("%s %s %s %s %s %s %s %s %s %s %s %s %s\n",
                hand[0], hand[1], hand[2], hand[3], hand[4], hand[5], hand[6],
                hand[7], hand[8], hand[9], hand[10], hand[11], hand[12])==13){
		
		C['C']=C['D']=C['H']=C['S']=F['C']=F['D']=F['H']=F['S']=0;
		Points=0;

		//rule1
		for(i=0;i<13;i++){
			card = hand[i][0];
			if(card=='A') { Points +=4; F[hand[i][1]]=1;}//suit stopped
			else if(card=='K') Points +=3;
			else if(card=='Q') Points +=2;
			else if(card=='J') Points++;
			C[hand[i][1]]++;	
		}
		for(i=0;i<13;i++){
			card = hand[i][0];
			suit = hand[i][1];
			if(card=='K'){ 
				if(C[suit]==1)Points--; //rule 2
				if(C[suit]>1)F[suit] = 1; //suit sttoped
			}
			else if(card=='Q'){ 
				if(C[suit]<=2)Points--; //rule 3
				if(C[suit]>2)F[suit] = 1; //suit sttoped
			}
			else if(card=='J' && C[suit]<=3) Points--; //rule 4
			
		}
		if((Points>=16) && F['C'] && F['S'] && F['H'] && F['D'])
			printf("BID NO-TRUMP\n");
			
		else{
			char CC=C['C'],CD=C['D'],CH=C['H'],CS=C['S'];
			if(CC==2)Points++;//rule 5
			else if(CC<2)Points+=2;//rule 6 and 7

			if(CD==2)Points++;//rule 5
			else if(CD<2)Points+=2;//rule 6 and 7

			if(CH==2)Points++;//rule 5
			else if(CH<2)Points+=2;//rule 6 and 7

			if(CS==2)Points++;//rule 5
			else if(CS<2)Points+=2;//rule 6 and 7
		
			if(Points<14) printf("PASS\n",Points);
			else{
				if(CS>=CH && CS>=CD && CS>=CC) printf("BID S\n");
				else if(CH>=CD && CH>=CC) printf("BID H\n");
				else if(CD>=CC) printf("BID D\n");
				else printf("BID C\n");
			}
		}
	}
	return 0;



}

