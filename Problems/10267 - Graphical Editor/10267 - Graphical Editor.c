#include <stdio.h>
#include <stdlib.h>

char table[253][253];
char c,c2;
int m,n;

void rec(int x, int y)
{
    if(x<0 || x==n || y<0 || y==m || table[y][x]!=c2)
        return;
    table[y][x] = c;
    rec(x+1,y);
    rec(x-1,y);
    rec(x,y+1);
    rec(x,y-1);
}

int main(void)
{
    int i,j,x,y,x2,y2;

    char name[1000];
    while(scanf(" %c ",&c)!=EOF && c!='X')
    {
        if(c=='I'){
            scanf(" %d %d ",&n,&m); /*OK*/
            for(i=0;i<m;i++){
                for(j=0;j<n;j++){
                    table[i][j]='O';
                }
            }
        }else if(c=='C'){ /*OK*/
             for(i=0;i<m;i++){
                for(j=0;j<n;j++){
                    table[i][j]='O';
                }
            }
        }else if(c=='L'){
            scanf(" %d %d %c ",&x,&y,&c); /*OK*/
            table[y-1][x-1]= c;
        }else if(c=='V'){ /*OK*/
            scanf(" %d %d %d %c ",&x,&y,&y2,&c);
            if(y>y2){ y2^=y; y^=y2; y2^=y;}
            x--;y--;
            for(j=y;j<y2;j++){
                table[j][x]= c;
            }
        }else if(c=='H'){ /*OK*/
            scanf(" %d %d %d %c ",&x,&x2,&y,&c);
            if(x>x2){ x2^=x; x^=x2; x2^=x;}
            x--;y--;
            for(i=x;i<x2;i++){
                table[y][i]= c;
            }
        }else if(c=='K'){ /*OK*/
            scanf(" %d %d %d %d %c ",&x,&y,&x2,&y2,&c);
            if(y>y2){ y2^=y; y^=y2; y2^=y;}
            if(x>x2){ x2^=x; x^=x2; x2^=x;}
            x--;y--;
            for(i=x;i<x2;i++){
                for(j=y;j<y2;j++){
                    table[j][i]= c;
                }
            }
        }else if(c=='F'){
            scanf(" %d %d %c ",&x,&y,&c);
            x--;y--;
            c2 = table[y][x];
            if(c!=c2)
                rec(x,y);
        }else if(c=='S')
        {
            scanf("%s",name);
            printf("%s\n",name);
            for(i=0;i<m;i++){
                for(j=0;j<n;j++){
                    printf("%c",table[i][j]);
                }
                printf("\n");
            }
        }
        else{
            gets(name);
        }
    }
    return 0;
}
