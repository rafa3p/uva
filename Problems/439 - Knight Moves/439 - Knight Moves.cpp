#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <queue>
using namespace std;

char visited[9][9];
char moves[8][2] =   {{1,2},{1,-2},
                     {-1,2},{-1,-2},
                     {2,1},{2,-1},
                     {-2,1},{-2,-1}};
struct state {
    char pos[2];
    int depth;
};
state end;

char equal(state a,state b){
    if(a.pos[0]==b.pos[0] && a.pos[1]==b.pos[1] )
        return 1;
    return 0;
}

void next_states(state a, state nexts[8]){
    for(int i=0;i<8;i++){
        nexts[i] = a;
        nexts[i].depth = a.depth+1;
        nexts[i].pos[0] += moves[i][0];
        nexts[i].pos[1] += moves[i][1];
    }
}

int BFS(state current){
    queue<state> q;
    state nexts[8];
    q.push(current);
    while(!q.empty()){
        current=q.front();q.pop();
        if(equal(current,end)) return current.depth;
        next_states(current,nexts);
        for(int i = 0;i<8;i++){
            /*Checking valid board place(1-8)*/
            if( nexts[i].pos[0]>0 && nexts[i].pos[0] < 9 &&
                nexts[i].pos[1]>0 && nexts[i].pos[1] < 9 &&
                !visited[nexts[i].pos[0]][nexts[i].pos[1]] ){
                    visited[nexts[i].pos[0]][nexts[i].pos[1]] = 1;
                    q.push(nexts[i]);
            }
        }
    }
    return -1;
}

int main(void){
    char a[3],b[3];
    state begin;

    while(scanf(" %s %s \n",a,b)==2){
        memset(visited, 0, sizeof(visited));/*9*9 positions*/
        begin.pos[0] = a[0] - 'a' + 1; /*Values between 1-8*/
        begin.pos[1] = a[1] - '0';
        end.pos[0] = b[0] - 'a' + 1;
        end.pos[1] = b[1] - '0';
        begin.depth = 0;
        visited[begin.pos[0]][begin.pos[1]] = 1;
        printf("To get from %s to %s takes %d knight moves.\n",
               a,b,BFS(begin));
    }
    return 0;
}
