#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <stack>
#include <algorithm>
using namespace std;

#define EPS 1e-10

struct point {
    int x,y;
    bool operator ==(const point &a) const {
        return x==a.x && y==a.y;
    }
    /*Operator to sort (to remove duplicates)*/
    bool operator <(const point &a) const {
        return (x < a.x || (x == a.x && y < a.y));
    }
};

point first_point;
vector<point> in;
typedef vector <point> polygon;

/*Evaluates the distance between the points a and b*/
double point_distance(point a,point b){
    double dx = a.x-b.x;
    double dy = a.y-b.y;
    return sqrt( dx*dx+dy*dy );
}
/*Cross product of vectors a-b and c-b*/
int cross_product(point a, point b, point c){
    return (c.x-b.x)*(a.y-b.y) - (c.y-b.y)*(a.x-b.x);
}
/*Return true if point c is in the right side of line ab*/
bool cw(point a, point b, point c){
    return cross_product(a,b,c) < 0;
}
/*Check whether the points a,b and c are collinear*/
bool collinear(point a, point b, point c){
    return cross_product(a,b,c) == 0;
}

bool smaller_angle(const point a, const point b){
    if(collinear(first_point,a,b))
        return point_distance(first_point,a) <= point_distance(first_point,b);
    double d1x = a.x - first_point.x;
    double d1y = a.y - first_point.y;
    double d2x = b.x - first_point.x;
    double d2y = b.y - first_point.y;
    return (atan2(d1y,d1x) - atan2(d2y,d2x)) < 0;
}

/*Debugging*/
void printPoints(vector<point> &vec){
    /*printf("POINTS\n");*/
    for(vector<point> :: iterator it = vec.begin();it!=vec.end();it++)
        printf("<%d,%d> ",it->x,it->y);
    printf("\n\n");
}

/*In each call, takes off the convex hull from 'in'*/
bool convex_hull(){
    int i,idx=0;
    int sz = in.size();
    /*printf("%d ",sz);Debugging*/
    if(sz<3)return false;

    /*Finding the point with the lowest x and placing at position 0
    If tie: choose lowest y*/
    for(i=1;i<sz;i++)
        if(in[i].x < in[idx].x || (in[i].x == in[idx].x && in[i].y < in[idx].y))
            idx = i;

    /*Placing the point in the first position*/
    point temp = in[idx]; in[idx] = in[0]; in[0] = temp;

    /*Sorting points by angle*/
    first_point = in[0];
    sort(++in.begin(),in.end(),smaller_angle);

    /*Building the convex hull*/
    polygon hull;
    hull.push_back(first_point);hull.push_back(in[1]);
    for(i=2;i<sz;i++){
        while(hull.size()>1 && cw(hull[hull.size()-2],hull[hull.size()-1],in[i]))
            hull.pop_back();
        hull.push_back(in[i]);
    }
    /*printPoints(hull);Debugging*/

    /*Removing elements from 'in' that are in 'hull'*/
    vector<point> :: iterator it1 = in.begin();
    for(vector<point> :: iterator it2 = hull.begin();it2!=hull.end();it2++){
        while(!(*it1 == *it2 ))
            it1++;
        in.erase(it1);
    }
    return hull.size()>2;/*True if the convex hull has at least three elements*/
}

int main(void){
    int n,layers;
    point input;
    vector<point> :: iterator it;
    while(scanf(" %d ",&n) && n !=0){
        if(!in.empty())in.clear();
        /*Reading n points*/
        while(n--){
            scanf(" %d %d ",&input.x,&input.y);
            in.push_back(input);
        }

        /*Removing duplicates
        sort(in.begin(),in.end());
        it = unique(in.begin(),in.end());
        in.resize(distance(in.begin(),it));*/

        /*Counting the number of layers*/
        for(layers=0;convex_hull();layers++){}
        /*printf("\nNumber of Layers : %d\n",layers);Debugging*/
        if(layers&1)printf("Take this onion to the lab!\n");
        else printf("Do not take this onion to the lab!\n");
    }
    return 0;
}



