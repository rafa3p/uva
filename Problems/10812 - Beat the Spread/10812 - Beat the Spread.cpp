#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define LL unsigned long long

int main(void)
{

	int t,a,b,s,d,sum;
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d %d",&s,&d);
		if(d>s || ((s+d)&1))printf("impossible\n");
		else printf("%d %d\n",((s+d)>>1),((s-d)>>1));
		
	
	}
	return 0;
}
