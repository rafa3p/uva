#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <numeric>
#define INT_MAX 2147483647
#define INT_MIN -2147483647
#define pi acos(-1.0)
#define N 1000000
#define LL unsigned long long
using namespace std;

int main(void)
{
	int Months,k;
	double DownPayment,Loan,CarValue,MonthlyPayment;
	double Depreciation[101];
	int i,j,m[101];
	while(scanf("%d %lf %lf %d",&Months,&DownPayment,&Loan,&k) && (Months>0)){
		CarValue = Loan+DownPayment;	
		MonthlyPayment = Loan/Months;	
		for(i=j=0;i<k;i++)
			scanf("%d %lf",&m[i],&Depreciation[i]);
		m[k]=200;//* util later
		
		i=j=0;	
		while(1){			
			CarValue *= (1-Depreciation[j]);
			if(CarValue > Loan) break;
			Loan -= MonthlyPayment; 
			if(m[j+1] <= ++i) j++; //now
		}
		
		if(i==1)printf("1 month\n");
		else printf("%d months\n",i);
	
	}	
	return 0;
}
